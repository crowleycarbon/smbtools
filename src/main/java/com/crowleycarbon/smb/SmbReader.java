/**
 =====================
 The MIT License (MIT)
 =====================

 Copyright (c) 2020 Crowley Carbon Ltd

 Permission is hereby granted, free of charge,
 to any person obtaining a copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:

 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

//
// History:
//  Mar 2020 John MacEnri Creation
//

package com.crowleycarbon.smb;

import com.hierynomus.msdtyp.AccessMask;
import com.hierynomus.msfscc.fileinformation.FileIdBothDirectoryInformation;
import com.hierynomus.mssmb2.SMB2CreateDisposition;
import com.hierynomus.mssmb2.SMB2CreateOptions;
import com.hierynomus.mssmb2.SMB2ShareAccess;
import com.hierynomus.smbj.SMBClient;
import com.hierynomus.smbj.SmbConfig;
import com.hierynomus.smbj.auth.AuthenticationContext;
import com.hierynomus.smbj.session.Session;
import com.hierynomus.smbj.share.DiskShare;
import com.hierynomus.smbj.share.File;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFilenameFilter;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SmbReader
{
    private SMBClient client = null;
    private Session session = null;
    private DiskShare share = null;
    private boolean usingSmbV1 = false;

    private String host = null;
    private String shareName = null;
    private NtlmPasswordAuthentication auth = null;

    public SmbReader(String host, String user, String pass, String domain, boolean useV1) throws IOException
    {
        this.host = host;
        if (useV1)
        {
            this.usingSmbV1 = true;
            this.auth = new NtlmPasswordAuthentication(domain, user, pass);
        }
        else
        {
            SmbConfig cfg = SmbConfig.builder().withMultiProtocolNegotiate(true).build();
            client = new SMBClient(cfg);
            AuthenticationContext ac = null;
            if (user == null || pass == null)
            {
                ac = AuthenticationContext.anonymous();
            }
            else
            {
                ac = new AuthenticationContext(user, pass.toCharArray(), domain);
            }
            try
            {
                session = client.connect(host).authenticate(ac);
            }
            catch(Exception e)
            {
                if (e.getMessage().contains("SMBv1 is not supported by SMBJ"))
                {
                    System.out.println("Forced to use SMBv1");
                    this.usingSmbV1 = true;
                    this.auth = new NtlmPasswordAuthentication(domain, user, pass);
                }
                else
                {
                    throw (e);
                }
            }
        }
    }

    public SmbReader connectShare(String shareName) throws IOException
    {
        this.shareName = shareName;
        if (share != null)
        {
            share.close();
        }
        if (!usingSmbV1)
        {
            share = (DiskShare) session.connectShare(shareName);
        }
        return this;
    }

    public String[] listFiles(String path, String pattern) throws MalformedURLException, SmbException
    {
        String[] ret = null;
        if (usingSmbV1)
        {
            String fullUrl = "smb://" + host + "/"+ shareName + "/" + path + "/";
            SmbFile dir = new SmbFile(fullUrl,auth);
            String regex = pattern.replace("?", ".?").replace("*", ".*?");

            SmbFilenameFilter filter = new SmbFilenameFilter()
            {
                @Override
                public boolean accept(SmbFile smbFile, String s) throws SmbException
                {
                    return s.matches(regex);
                }
            };

            final SmbFile[] files = dir.listFiles(filter);
            ret = new String[files.length];
            for (int i = 0; i < files.length; i++)
            {
                ret[i] = files[i].getName();
            }
        }
        else
        {
            List<FileIdBothDirectoryInformation> files = share.list(path, pattern);
            ret = new String[files.size()];
            for (int i = 0; i < files.size(); i++)
            {
                ret[i] = files.get(i).getFileName();
            }
        }
        return ret;
    }

    private byte[] getFileByteArray(String path, String filename) throws IOException
    {
        byte[] ret = null;
        InputStream is = null;
        if (usingSmbV1)
        {
            String fullUrl = "smb://" + host + "/"+ shareName + "/" + path + "/" + filename;
            SmbFile file = new SmbFile(fullUrl,auth);
            ret = new byte[(int) file.length()];
            is = new SmbFileInputStream(file);
            is.read(ret);
        }
        else
        {
            String filePath = filename;
            if (path != null && path.length() > 0) filePath = path + "/" + filename;
            Set<SMB2ShareAccess> s = EnumSet.of(SMB2ShareAccess.FILE_SHARE_READ);
            File file = share.openFile(filePath, EnumSet.of(AccessMask.GENERIC_READ), null, s, SMB2CreateDisposition.FILE_OPEN, null);
            ret = new byte[(int) file.getFileInformation().getStandardInformation().getEndOfFile()];
            is = file.getInputStream();
            is.read(ret);
            file.close();
        }
        is.close();
        return ret;
    }

    public byte[] readBinaryFile(String path, String filename) throws IOException
    {
        return getFileByteArray(path, filename);
    }

    public String readTextFile(String path, String filename) throws IOException
    {
        return new String(getFileByteArray(path, filename), "UTF-8");
    }

    public void moveFile(String path, String filename, String newPath, String newFilename) throws IOException
    {
        if (path.equals(newPath) && filename.equals(newFilename)) return;

        if (usingSmbV1)
        {
            String fromUrl = "smb://" + host + "/"+ shareName + "/" + path + "/" + filename;
            SmbFile smbFromFile = new SmbFile(fromUrl,auth);
            if (!path.equals(newPath))
            {
                String toDirUrl = "smb://" + host + "/"+ shareName + "/" + newPath + "/";
                SmbFile toDir = new SmbFile(toDirUrl,auth);
                if (!toDir.exists()) toDir.mkdir();
            }
            String toUrl = "smb://" + host + "/"+ shareName + "/" + newPath + "/" + newFilename;
            SmbFile smbToFile = new SmbFile(toUrl,auth);
            smbFromFile.renameTo(smbToFile);
        }
        else
        {
            String fromFullPath = filename;
            if (path != null && path.length() > 0) fromFullPath = path + "/" + filename;

            String toFullPath = newFilename;
            if (newPath != null && newPath.length() > 0) toFullPath = newPath + "/" + newFilename;

            if (!path.equals(newPath))
            {
                if (!share.folderExists(newPath)) share.mkdir(newPath);
            }

            //Set<SMB2ShareAccess> s = EnumSet.of(SMB2ShareAccess.FILE_SHARE_READ);
            //File file = share.openFile(fromFullPath, EnumSet.of(AccessMask.GENERIC_READ), null, s, SMB2CreateDisposition.FILE_OPEN, null);

            Set<SMB2ShareAccess> shareAccess = new HashSet<>();
            shareAccess.addAll(SMB2ShareAccess.ALL);

            Set<SMB2CreateOptions> createOptions = new HashSet<SMB2CreateOptions>();
            createOptions.add(SMB2CreateOptions.FILE_WRITE_THROUGH);

            Set<AccessMask> accessMaskSet = new HashSet<AccessMask>();
            accessMaskSet.add(AccessMask.GENERIC_ALL);

            File file = share.openFile(fromFullPath, accessMaskSet, null, shareAccess, SMB2CreateDisposition.FILE_OPEN, createOptions);

            file.rename(toFullPath, true);
            file.close();
        }
    }

    public void close()
    {
        if (client != null) this.client.close();
    }

    public static void main(String[] args)
    {
        if (args.length < 4)
        {
            System.out.println("Usage: SmbReader <hostname|ip-address> <username> <password> <shareName> [<domain>] [<useV1>] [<pathInShare>] [<fileFilterPattern>]");
            System.exit(-1);
        }
        try
        {
            String domain = null;
            if (args.length > 4) domain = args[4];

            boolean useV1 = false;
            if (args.length > 5) useV1 = args[5].equals("true");

            SmbReader smbReader = new SmbReader(args[0], args[1], args[2], domain, useV1);
            smbReader.connectShare(args[3]);

            String path = "";
            if (args.length > 6) path = args[6];

            String pattern = "*.csv";
            if (args.length > 7) pattern = args[7];

            String[] files = smbReader.listFiles(path, pattern);
            System.out.println("There were " + files.length + " files matching the filter " + pattern + " in path " + path);
            if (files.length > 0)
            {
                System.out.println("First file name is: " + files[0]);
                //byte[] fc = smbReader.readBinaryFile(path, files[0]);
                String fc = smbReader.readTextFile(path, files[0]);
                System.out.println("First file content:\n" + fc);
                String suffix = "";
                String toDir = "archive/";
                smbReader.moveFile(path, files[0], path + toDir, files[0] + suffix);
                System.out.println("First file moved to :\n" + path + toDir + files[0] + suffix);
            }

            smbReader.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
