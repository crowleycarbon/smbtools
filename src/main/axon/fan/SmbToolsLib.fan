/**
=====================
The MIT License (MIT)
=====================

Copyright (c) 2020 Crowley Carbon Ltd

Permission is hereby granted, free of charge,
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
**/

//
// History:
//  Mar 2020 John MacEnri Creation
//

using [java] com.crowleycarbon.smb::SmbReader
using [java] java.nio::ByteBuffer
using [java] fanx.interop::Interop
using [java] fanx.interop::ByteArray
using haystack
using axon
using skyarcd

const class SmbToolsLib
{
  **
  ** Construct an SMB Reader.
  **
  ** Examples:
  **   smbReaderOpen("10.0.0.1")
  **   smbReaderOpen("10.0.0.1", "user", "pass")
  **   smbReaderOpen("10.0.0.1", "user", "pass", "DOMAIN")
  **
  ** Side effects:
  **   - Returns an instance of SmbReader which must be passed in on all subsequent calls that wish to use this reader
  **
  @Axon static SmbReader smbReaderOpen(Str host, Str? user := null, Str? pass := null, Str? domain := null, Bool? useSmbV1 := false)
  {
    return SmbReader(host, user, pass, domain, useSmbV1)
  }

  **
  ** Use the provided SmbReader instance to connect to a specific share name.
  ** The same SmbReader can be re-used for connecting to different shares, but just one at a time.
  ** Each time smbReaderConnect is called, the previous share connection is released.
  **
  ** Examples:
  **   smbReaderOpen("10.0.0.1").smbReaderConnect("shareName")
  **
  ** Side effects:
  **   - Returns the same SmbReader instance ready for subsequent calls against the shareName now connected
  **
  @Axon static SmbReader smbReaderConnect(SmbReader smb, Str shareName)
  {
    return smb.connectShare(shareName)
  }

  **
  ** Use the provided SmbReader instance to list files in a specific folder path, that match
  ** the provided file name pattern.
  ** The SmbReader must previously have been connected to a specific shareName using smbReaderConnect.
  **
  ** The same SmbReader can be re-used for connecting to different shares, but just one at a time.
  ** Each time smbReaderConnect is called, the previous share connection is released.
  **
  ** Examples:
  **   smbReaderOpen("10.0.0.1").smbReaderConnect("shareName").smbReaderListFiles("dirName", "*.txt")
  **
  ** Side effects:
  **   - Returns a list of file names that matched the provided pattern in the folder name provided.
  **
  @Axon static Str[] smbReaderListFiles(SmbReader smb, Str path, Str pattern)
  {
     return smb.listFiles(path, pattern)
  }

  **
  ** Use the provided SmbReader to read the contents of the named text file in the named folder.
  ** The file contents will be returned as a Str and can be further parsed by the calling Axon
  **
  ** Examples:
  **   smbReaderOpen("10.0.0.1").smbReaderConnect("shareName").smbReaderReadFile("dirName", "file1.txt")
  **
  ** Side effects:
  **   - Returns a list of file names that matched the provided pattern in the folder name provided.
  **
  @Axon static Str smbReaderReadFile(SmbReader smb, Str path, Str filename)
  {
    return smb.readTextFile(path, filename)
  }

  **
  ** Use the provided SmbReader to read the contents of the named binary file in the named folder.
  ** The file contents will be returned as a Fantom Buf which can be passed to a function like poiReaderOpenBuf
  ** which understands how to parse a binary byte stream.
  **
  ** Examples:
  **   smbReaderOpen("10.0.0.1").smbReaderConnect("shareName").smbReaderReadBinaryFile("dirName", "file1.xlsx")
  **
  ** Side effects:
  **   - Returns a binary Buf instance which can be used in further calls
  **
  @Axon static Buf smbReaderReadBinaryFile(SmbReader smb, Str path, Str filename)
  {
    return Interop.toFan(ByteBuffer.wrap(smb.readBinaryFile(path, filename)))
  }

  **
  ** Use the provided SmbReader to cause the remote moving or renaming of a file.
  **
  ** which understands how to parse a binary byte stream.
  **
  ** Examples:
  **   smbReaderOpen("10.0.0.1").smbReaderConnect("shareName").smbReaderMoveFile("dirName1", "file1.xlsx", "dirName2", "file3.xlsx")
  **
  ** Side effects:
  **   - If the path and newPath are different, the file will have changed directory location on the remote share
  **   - If the filename and newFilename are different, the file will have changed its name
  **   - If path and newPath are the same, only the filename will change
  **   - If path and newPath are the same and also filename and newFilename are the same, nothing will happen
  **
  @Axon static Void smbReaderMoveFile(SmbReader smb, Str path, Str filename, Str newPath, Str newFilename)
  {
    smb.moveFile(path, filename, newPath, newFilename)
  }

  **
  ** Close the provided SmbReader along with any of its share or file connections.
  **
  ** Examples:
  **   smbReaderOpen("10.0.0.1").smbReaderConnect("shareName").smbReaderClose
  **
  ** Side effects:
  **   - Closes completely the provided SmbReader instance.
  **
  @Axon static Void smbReaderClose(SmbReader smb)
  {
    smb.close
  }

}

